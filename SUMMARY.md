# Summary

## 2.0 专题
* [2.0更新汇总](changes-20.md)
    * [表结构变化](change-20-tables.md)

-----
* [Introduction](README.md)
* [GitBook使用](chapter0.md)
* [开发环境搭建](chapter1.md)
    * [创建数据库](chapter1.2.md)
    * [Oracle,MySql,Sqlserver数据库配置](chapter1.1.md)
* [新建项目](chapter2.md)
    * [CAS集成](chapter2.1.md)
    * [LDAP验证](ldap-config.md)
        * [LDAP服务器搭建](ldap-server.md)
* [开发流程](chapter3.md)
    * [发布 WebService](webservice-cxf.md)
* [框架介绍](chapter4.md)
    * [配置维护](profile.md)
    * [消息机制](message.md)
* [框架功能描述](框架功能描述.md)
    * [用户功能描述](sys_user.md)
* [后端开发规范](chapter5.md)
    * [编码规范](coding_spec.md)
    * [Checkstyle](checkstyle.md)
* [前端开发手册 \(2.0\)](front_dev_principle-20.md)
    * [kendoUI form](chapter9.1.md)
    * [kendoUI grid](chapter9.2.md)

-----
* [后端开发](chapter7.md)
    * [数据多语言](chapter7.1.md)
    * [MyBatis 使用手册](chapter7.2.md)
    * [Maven 使用手册](chapter7.3.md)
* [部署](chapter8.md)

