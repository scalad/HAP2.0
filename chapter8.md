# 部署

# tomcat


### 1. 关闭 eclipse server `Auto Reload` 特性

  * 禁用默认值
<img src="assets/disable_autoreload_all.png" width="50%"/>

  * 单独禁用项
<img src="assets/disable_autoreload_1.png" width="50%"/>

* 建议关闭 jar 扫描, 提高启动速度, 减少内存占用 , 修改 `catalina.properties `

```properties
tomcat.util.scan.StandardJarScanFilter.jarsToSkip=*.jar
```

### 2. 内存设置

 * Linux  修改 `catalina.sh` ,开始处添加

    ```bash
        JAVA_OPTS='-Xms512m -Xmx2048m'
    ```
 * Windows 修改 `catalina.bat`
    
    ```bash
        set JAVA_OPTS=-Xms512m -Xmx2048m 
    ```

### 3. 启用远程 JMX 连接

 修改 catalina.sh , 搜索 `[ "$1" = "start" ]` , 在下方添加

```bash
JAVA_OPTS="-Dcom.sun.management.jmxremote \
-Dcom.sun.management.jmxremote.port=1099 \
-Djava.rmi.server.hostname=192.168.1.111 \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.authenticate=false $JAVA_OPTS"
```

> linux 系统可以用 `` `hostname -i` `` 获取 ip