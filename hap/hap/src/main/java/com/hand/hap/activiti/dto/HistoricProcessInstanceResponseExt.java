package com.hand.hap.activiti.dto;

import org.activiti.rest.service.api.history.HistoricProcessInstanceResponse;

/**
 * @author shengyang.zhou@hand-china.com
 */
public class HistoricProcessInstanceResponseExt extends HistoricProcessInstanceResponse {
    private String processName;

    private String startUserName;

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getStartUserName() {
        return startUserName;
    }

    public void setStartUserName(String startUserName) {
        this.startUserName = startUserName;
    }
}
