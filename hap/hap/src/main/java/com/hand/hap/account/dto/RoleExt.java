package com.hand.hap.account.dto;

/**
 * @author shengyang.zhou@hand-china.com
 */
public class RoleExt extends Role {
    private Long surId;

    public Long getSurId() {
        return surId;
    }

    public void setSurId(Long surId) {
        this.surId = surId;
    }
}
