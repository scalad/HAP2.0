package com.hand.hap.intergration.controllers;


import com.codahale.metrics.annotation.Timed;
import com.hand.hap.account.exception.UserException;
import com.hand.hap.intergration.dto.HapInterfaceHeader;
import com.hand.hap.intergration.service.IHapInterfaceHeaderService;
import com.hand.hap.cache.impl.ApiConfigCache;
import com.hand.hap.core.IRequest;
import com.hand.hap.system.controllers.BaseController;
import com.hand.hap.system.dto.ResponseData;
import com.hand.hap.system.service.ICodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author jiguang.sun@hand-china.com
 *         xiangyu.qi@hand-china.com 2016/11/01
 * @version 2016/7/21
 */

@Controller
@RequestMapping("/sys/api")
public class HapInterfaceHeaderController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(HapInterfaceHeaderController.class);

    @Autowired
    private IHapInterfaceHeaderService headerService;

    @Autowired
    private ApiConfigCache apiCache;


    @Autowired
    private ICodeService codeService;


    /**
     * 获取所有的系统路径
     */
    @RequestMapping(value = "/queryAllHeader", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData getHeaderList(HttpServletRequest request, @ModelAttribute HapInterfaceHeader headerAndHeaderTlDTO
       ,@RequestParam(defaultValue = DEFAULT_PAGE) final int page, @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) final int pagesize) {

        IRequest iRequest = createRequestContext(request);
        List<HapInterfaceHeader> list = headerService.getAllHeader(iRequest, headerAndHeaderTlDTO, page, pagesize);
        return new ResponseData(list);

    }

    /**
     * 查询所有系统类型
     *
     * @param request
     * @return
     * @throws UserException
     */
   /* @RequestMapping(value = "/queryAllHeader", method = RequestMethod.GET)
    @ResponseBody
    @Timed
    public ResponseData getAllSystemType(HttpServletRequest request) throws UserException {
        IRequest iRequest = createRequestContext(request);


        CodeValue codeValue = new CodeValue();
        Long id = Long.parseLong("135");
        codeValue.setCodeId(id);
        List<CodeValue> list = codeService.selectCodeValues(iRequest, codeValue);

        return new ResponseData(list);
    }*/

    /*
    * 新增 HmsInterfaceHeader
    * */
    @RequestMapping(value = "/addHeader", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData addHeader(HttpServletRequest request, @RequestBody(required = false) HapInterfaceHeader hmsInterfaceHeader) {
        logger.info("add HmsInterfaceHeader and HmsInterfaceHeaderTl:{} ", hmsInterfaceHeader.getInterfaceCode());
        IRequest iRequest = createRequestContext(request);
        hmsInterfaceHeader.setHeaderId(UUID.randomUUID().toString());
        hmsInterfaceHeader.setDescription(hmsInterfaceHeader.getName());
        HapInterfaceHeader hmsInterfaceHeaderNew = headerService.insert(iRequest, hmsInterfaceHeader);

        if (hmsInterfaceHeaderNew != null) {
            return new ResponseData();
        } else {
            return new ResponseData(false);
        }

    }

    /*
    * 更新 HeaderAndHeaderTlDTO
    * */
    @RequestMapping(value = "/updateHeader", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData updateHeader(HttpServletRequest request, @RequestBody(required = false) HapInterfaceHeader hmsInterfaceHeader) {
        logger.info("update HmsInterfaceHeader  headerId:{} ", hmsInterfaceHeader.getHeaderId());
        IRequest iRequest = createRequestContext(request);

        hmsInterfaceHeader.setDescription(hmsInterfaceHeader.getName());

        int result = headerService.updateHeader(iRequest, hmsInterfaceHeader);

        if (result > 0) {
            return new ResponseData();
        } else {
            return new ResponseData(false);
        }


    }

    /*
    * 根据headerId 查询 header and line
    * */
    @RequestMapping(value = "/getHeaderAndLine", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData getHeaderAndLine(HttpServletRequest request, @RequestBody(required = false) HapInterfaceHeader headerAndHeaderTlDTO) {

        IRequest iRequest = createRequestContext(request);

        return new ResponseData(headerService.getHeaderAndLineList(iRequest,headerAndHeaderTlDTO));

    }

    /*
    * 根据headerId获取header
    * */
    @RequestMapping(value = "/getHeaderByHeaderId", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData getHeaderByHeaderId(HttpServletRequest request, @RequestBody(required = false) HapInterfaceHeader headerAndHeaderTlDTO) {

        IRequest iRequest = createRequestContext(request);

        return new ResponseData(headerService.getHeaderByHeaderId(iRequest,headerAndHeaderTlDTO));
    }


    /*
    * 根据lineId获取headerAndLine
    * */
    @RequestMapping(value = "/getHeaderAndLineByLineId", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData getHeaderAndLineByLineId(HttpServletRequest request, @RequestBody(required = false) HapInterfaceHeader headerAndLineDTO) {
        logger.info("getHeaderAndLineByLineId lineId:{}", headerAndLineDTO.getLineId());
        HapInterfaceHeader headerAndLineDTO1 = headerService.getHeaderAndLineByLineId(headerAndLineDTO);

        return new ResponseData(Arrays.asList(headerAndLineDTO1));
    }

    /*
    * 所有有效的请求
    * */
    @RequestMapping(value = "/getAllHeaderAndLine", method = RequestMethod.POST)
    @ResponseBody
    @Timed
    public ResponseData getAllHeaderAndLine(HttpServletRequest request,
                                            @RequestParam(defaultValue = DEFAULT_PAGE) final int page, @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) final int pagesize) {

        return new ResponseData(headerService.getAllHeaderAndLine(page,pagesize));
    }


}
