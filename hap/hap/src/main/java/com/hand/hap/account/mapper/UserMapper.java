/*
 * #{copyright}#
 */

package com.hand.hap.account.mapper;

import java.util.List;

import com.hand.hap.account.dto.User;
import com.hand.hap.mybatis.common.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author wuyichu
 */
public interface UserMapper extends Mapper<User> {

    User selectByUserName(String userName);

    List<User> selectByIdList(List<Long> userIds);

    int updatePassword(@Param("userId") Long userId,@Param("password") String passwordNew);

    int updatePasswordDate(@Param("userId") Long userId);

    int updateUserInfo(User user);
}