package com.hand.hap.hr.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.hand.hap.core.IRequest;
import com.hand.hap.hr.dto.Employee;
import com.hand.hap.hr.mapper.EmployeeMapper;
import com.hand.hap.hr.service.IEmployeeService;
import com.hand.hap.message.IMessagePublisher;
import com.hand.hap.system.service.impl.BaseServiceImpl;

/**
 * @author yuliao.chen@hand-china.com
 */
@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {

	@Autowired
    EmployeeMapper employeeMapper;
	
    @Autowired
    private IMessagePublisher messagePublisher;
    

    @Override
    public List<Employee> batchUpdate(IRequest request, List<Employee> list) {
        super.batchUpdate(request, list);
        for (Employee e : list) {
            messagePublisher.publish("employee.change", e);
        }
        return list;
    }


	@Override
	public List<Employee> queryAll(IRequest requestContext, Employee employee, int page, int pagesize) {
		PageHelper.startPage(page, pagesize);
		return employeeMapper.queryAll(employee);
	}
}
