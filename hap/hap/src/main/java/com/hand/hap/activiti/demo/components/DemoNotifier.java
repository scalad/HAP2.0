package com.hand.hap.activiti.demo.components;

import com.hand.hap.activiti.listeners.IUserTaskNotifier;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author shengyang.zhou@hand-china.com
 */
@Component
public class DemoNotifier implements IUserTaskNotifier {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onTaskCreate(TaskEntity task, UserEntity userEntity) {
        String message = userEntity.getFirstName() + " 你好:<br/>" + "你有一个工作流需要审批.";
        logger.debug(message);

    }
}
