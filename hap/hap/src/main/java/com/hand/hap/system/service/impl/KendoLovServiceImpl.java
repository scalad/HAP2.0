package com.hand.hap.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.runtime.StringGroovyMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hand.hap.cache.impl.LovCache;
import com.hand.hap.core.BaseConstants;
import com.hand.hap.system.dto.Lov;
import com.hand.hap.system.dto.LovItem;
import com.hand.hap.system.mapper.LovItemMapper;
import com.hand.hap.system.mapper.LovMapper;
import com.hand.hap.system.service.IKendoLovService;
import com.ibm.wsdl.Constants;

/**
 * KendoLov
 * 
 * @author njq.niu@hand-china.com
 *
 */
@Service
@Transactional
public class KendoLovServiceImpl implements IKendoLovService {

    private final Logger logger = LoggerFactory.getLogger(KendoLovServiceImpl.class);

    @Autowired
    private LovMapper lovMapper;

    @Autowired
    private LovItemMapper lovItemMapper;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LovCache lovCache;

    public String getLov(String contextPath, Locale locale, String lovCode) {
        LovEditor editor = getLovEditor(contextPath, locale, lovCode);
        try {
            return objectMapper.writeValueAsString(editor);
        } catch (JsonProcessingException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
        }
        return "''";
    }

    private LovEditor getLovEditor(String contextPath, Locale locale, String lovCode) {
        Lov lov = lovCache.getValue(lovCode);
        if (lov == null) {
            lov = lovMapper.selectByCode(lovCode);
            if (lov != null) {
                List<LovItem> items = lovItemMapper.selectByLovId(lov.getLovId());
                lov.setLovItems(items);
                lovCache.setValue(lov.getCode(), lov);
            }
        }

        return lov != null ? createLovEditor(contextPath, locale, lov, lov.getLovItems()) : null;
    }

    private LovEditor createLovEditor(String contextPath, Locale locale, Lov lov, List<LovItem> items) {
        LovEditor editor = new LovEditor(lov, locale);
        editor.setGrid(new LovGrid(contextPath, locale, lov, items));
        buildConditionForm(editor, items, contextPath, locale);
        return editor;
    }

    private void buildConditionForm(LovEditor editor, List<LovItem> items, String contextPath, Locale locale) {
        StringBuilder html = new StringBuilder();

        List<LovItem> formItems = new ArrayList<>();
        for (LovItem item : items) {
            if (BaseConstants.YES.equalsIgnoreCase(item.getConditionField())) {
                formItems.add(item);

                String field = StringUtils.defaultIfEmpty(item.getConditionFieldName(), item.getGridFieldName());
                html.append("<div class='form-group'>").append("<label class='col-sm-3 control-label'>")
                        .append(messageSource.getMessage(item.getDisplay(), null, item.getDisplay(), locale))
                        .append("</label>").append("<div class='col-sm-9'>")
                        .append("<input name='" + field + "' data-bind='value:data." + field + "' style='width:100%'>")
                        .append("</div>").append("</div>");

                editor.getFormItemMap().put(field, buildConditionFormItem(item, contextPath));
            }
        }
        formItems.sort((a, b) -> {
            return a.getConditionFieldSequence() - b.getConditionFieldSequence();
        });

        editor.setForm(html.toString());
    }

    /**
     * 暂定text和combobox两种组件.
     * 
     * @param item
     * @param contextPath
     * @return
     */
    private LovFormItem buildConditionFormItem(LovItem item, String contextPath) {
        LovFormItem formItem = new LovFormItem();
        String type = StringUtils.defaultIfEmpty(item.getConditionFieldType(), "text");
        switch (type) {
        case "text":
            formItem.setType("kendoMaskedTextBox");
            break;
        case "select":
            formItem.setType("kendoComboBox");
            if (StringUtils.isNotEmpty(item.getConditionFieldSelectCode())) {
                formItem.setDataSource(contextPath + "/common/code/" + item.getConditionFieldSelectCode() + "/");
                formItem.setDataValueField("value");
                formItem.setDataTextField("meaning");
            } else if (StringUtils.isNotEmpty(item.getConditionFieldSelectUrl())) {
                formItem.setDataSource(contextPath + item.getConditionFieldSelectUrl());
                formItem.setDataValueField(item.getConditionFieldSelectVf());
                formItem.setDataTextField(item.getConditionFieldSelectTf());
            }
            break;
        default:
            break;
        }
        return formItem;
    }

    private class LovEditor {

        LovEditor(Lov lov, Locale locale) {
            if (lov != null) {
                setTitle(messageSource.getMessage(lov.getTitle(), null, lov.getTitle(), locale));
                setWidth(lov.getWidth());
                setHeight(lov.getHeight());
                setDataTextField(lov.getTextField());
                setDataValueField(lov.getValueField());
                setPlaceholder(messageSource.getMessage(lov.getPlaceholder(), null, lov.getTitle(), locale));
            }
            if (BaseConstants.YES.equals(lov.getEditableFlag())) {
                setReadonly(false);
            }else{
                setReadonly(true);
            }
        }

        @JsonInclude(Include.NON_NULL)
        private Integer height;

        @JsonInclude(Include.NON_NULL)
        private Integer width;

        private boolean readonly;

        private String dataValueField;

        private String dataTextField;

        @JsonInclude(Include.NON_NULL)
        private String title;

        @JsonInclude(Include.NON_NULL)
        private String placeholder;

        private String form;

        private Map<String, LovFormItem> formItemMap = new HashMap<>();

        private LovGrid grid;

        public boolean isReadonly() {
            return readonly;
        }

        public void setReadonly(boolean readonly) {
            this.readonly = readonly;
        }

        public Map<String, LovFormItem> getFormItemMap() {
            return formItemMap;
        }

        public void setFormItemMap(Map<String, LovFormItem> formItemMap) {
            this.formItemMap = formItemMap;
        }

        public String getForm() {
            return form;
        }

        public void setForm(String form) {
            this.form = form;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public String getDataValueField() {
            return dataValueField;
        }

        public void setDataValueField(String dataValueField) {
            this.dataValueField = dataValueField;
        }

        public String getDataTextField() {
            return dataTextField;
        }

        public void setDataTextField(String dataTextField) {
            this.dataTextField = dataTextField;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public LovGrid getGrid() {
            return grid;
        }

        public void setGrid(LovGrid grid) {
            this.grid = grid;
        }

        public String getPlaceholder() {
            return placeholder;
        }

        public void setPlaceholder(String placeholder) {
            this.placeholder = placeholder;
        }

    }

    private class LovFormItem {

        private String type;

        @JsonInclude(Include.NON_NULL)
        private String dataSource;

        @JsonInclude(Include.NON_NULL)
        private String dataValueField;

        @JsonInclude(Include.NON_NULL)
        private String dataTextField;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDataSource() {
            return dataSource;
        }

        public void setDataSource(String dataSource) {
            this.dataSource = dataSource;
        }

        public String getDataValueField() {
            return dataValueField;
        }

        public void setDataValueField(String dataValueField) {
            this.dataValueField = dataValueField;
        }

        public String getDataTextField() {
            return dataTextField;
        }

        public void setDataTextField(String dataTextField) {
            this.dataTextField = dataTextField;
        }

    }

    private class LovGrid {

        LovGrid(String contextPath, Locale locale, Lov lov, List<LovItem> items) {
            setUrl(contextPath + "/common/lov/" + lov.getCode());
            setHeight(lov.getHeight());

            for (LovItem item : items) {
                if (BaseConstants.YES.equalsIgnoreCase(item.getGridField())) {
                    addColumn(new LovGridColumn(contextPath, locale, item));
                }
            }
            columns.sort((a, b) -> {
                return a.getSequence() - b.getSequence();
            });

        }

        private Integer height;

        private String url;

        private List<LovGridColumn> columns = new ArrayList<>();

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void addColumn(LovGridColumn column) {
            columns.add(column);
        }

        public List<LovGridColumn> getColumns() {
            return columns;
        }

        public void setColumns(List<LovGridColumn> columns) {
            this.columns = columns;
        }

    }

    private class LovGridColumn {

        LovGridColumn(String contextPath, Locale locale, LovItem item) {
            String display = messageSource.getMessage(item.getDisplay(), null, item.getDisplay(), locale);
            setTitle(display);
            setField(item.getGridFieldName());
            setAlign(item.getGridFieldAlign());
            setSequence(item.getGridFieldSequence());
            setWidth(item.getGridFieldWidth());

        }

        @JsonIgnore
        private Integer sequence = 1;

        private String field;

        @JsonInclude(Include.NON_NULL)
        private String title;

        @JsonInclude(Include.NON_NULL)
        private Integer width;

        @JsonInclude(Include.NON_NULL)
        private Map<String, Object> attributes;

        public Map<String, Object> getAttributes() {
            return attributes;
        }

        public void setAttributes(Map<String, Object> attributes) {
            this.attributes = attributes;
        }

        public void setAlign(String align) {
            Map<String, Object> attributes = getAttributes();
            if (attributes == null) {
                attributes = new HashMap<>();
                setAttributes(attributes);
            }
            attributes.put("style", "text-align:" + align);
        }

        public Integer getSequence() {
            return sequence;
        }

        public void setSequence(Integer sequence) {
            this.sequence = sequence;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

    }

}
