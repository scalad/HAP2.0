package com.hand.hap.db

import com.hand.hap.liquibase.MigrationHelper
dbType = MigrationHelper.getInstance().dbType()
databaseChangeLog(logicalFilePath:"patch.groovy"){

    changeSet(author: "xiangyuqi", id: "20161009-xiangyuqi-1") {
        addColumn(tableName:"SYS_USER"){
            column(name:"LAST_LOGIN_DATE",type:"datetime",remarks:"最后一次登录时间")
            column(name:"LAST_PASSWORD_UPDATE_DATE",type:"datetime",remarks:"最后一次修改密码时间")
        }
    }
    changeSet(author: "zhizheng.yang", id: "20161025-zhizheng.yang-1") {
        addColumn(tableName:"SYS_LOV"){
            column(name:"CUSTOM_SQL",type:"clob",remarks:"自定义sql")
        }
    }

}
