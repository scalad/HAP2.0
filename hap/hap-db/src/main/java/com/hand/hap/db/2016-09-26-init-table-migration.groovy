import com.hand.hap.liquibase.MigrationHelper

def mhi = MigrationHelper.getInstance()

databaseChangeLog(logicalFilePath:"2016-09-26-init-migration.groovy"){


    changeSet(author: "niujiaqing", id: "20160926-niujiaqing-hr-employee") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'HR_EMPLOYEE_S', startValue:"10001")
        }
        createTable(tableName: "HR_EMPLOYEE") {
            column(autoIncrement: "true", startWith:"10001", name: "EMPLOYEE_ID", type: "BIGINT", remarks: "员工ID") {
                constraints(nullable: "false", primaryKey: "true" ,primaryKeyName:"HR_EMPLOYEE_PK")
            }
            column(name: "EMPLOYEE_CODE", remarks: "员工编码", type: "VARCHAR(30)") {
                constraints(nullable: "false",unique:"true",uniqueConstraintName:"HR_EMPLOYEE_U1")
            }
            column(name: "NAME", remarks: "员工姓名", type: "VARCHAR(50)"){constraints(nullable: "false")}
            column(name: "BORN_DATE", remarks: "出生日期", type: "DATE")
            column(name: "EMAIL", remarks: "电子邮件", type: "VARCHAR(50)")
            column(name: "MOBIL", remarks: "移动电话", type: "VARCHAR(50)")
            column(name: "JOIN_DATE", remarks: "入职日期", type: "DATE")
            column(name: "GENDER", remarks: "性别", type: "VARCHAR(1)")
            column(name: "CERTIFICATE_ID", remarks: "ID", type: "VARCHAR(100)") {constraints(nullable: "false",unique:"true",uniqueConstraintName:"HR_EMPLOYEE_U2")}
            column(name: "STATUS", remarks: "状态", type: "VARCHAR(50)"){constraints(nullable: "false")}
            column(name: "ENABLED_FLAG", remarks: "启用状态", type: "VARCHAR(1)", defaultValue : "Y") {constraints(nullable: "false")}
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")
        }
    }
    
    changeSet(author: "niujiaqing", id: "20160926-niujiaqing-hr-org-unit") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'HR_ORG_UNIT_B_S', startValue:"10001")
        }
        createTable(tableName: "HR_ORG_UNIT_B") {
            column(autoIncrement: "true", startWith:"10001", name: "UNIT_ID", type: "BIGINT", remarks: "组织ID") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "PARENT_ID", remarks: "父组织", type: "BIGINT")
            column(name: "UNIT_CODE", remarks: "组织编码", type: "VARCHAR(50)")
            column(name: "NAME", remarks: "组织名称", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "组织描述", type: "VARCHAR(255)")
            column(name: "MANAGER_POSITION", remarks: "组织管理岗位", type: "BIGINT")
            column(name: "COMPANY_ID", remarks: "公司ID", type: "BIGINT")
            column(name: "ENABLED_FLAG", remarks: "启用状态", type: "VARCHAR(1)", defaultValue : "Y") {constraints(nullable: "false")}
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
        
        createTable(tableName: "HR_ORG_UNIT_TL") {
            column(name: "UNIT_ID", type: "BIGINT", remarks: "组织ID") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "LANG", remarks: "语言", type: "VARCHAR(50)") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "NAME", remarks: "组织名称", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "组织描述", type: "VARCHAR(255)")
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
    }

    changeSet(author: "niujiaqing", id: "20160926-niujiaqing-hr-org-position") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'HR_ORG_POSITION_B_S',startValue:"10001")
        }
        createTable(tableName: "HR_ORG_POSITION_B") {
            column(autoIncrement: "true",startWith:"10001", name: "POSITION_ID", type: "BIGINT", remarks: "岗位ID") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "UNIT_ID", remarks: "组织ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "POSITION_CODE", remarks: "岗位编码", type: "VARCHAR(50)")
            column(name: "NAME", remarks: "岗位名称", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "岗位描述", type: "VARCHAR(255)")
            column(name: "PARENT_POSITION_ID", remarks: "父岗位ID", type: "BIGINT")
            column(name: "ENABLED_FLAG", remarks: "启用状态", type: "VARCHAR(1)", defaultValue : "Y") {constraints(nullable: "false")}
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
        
        createTable(tableName: "HR_ORG_POSITION_TL") {
            column(name: "POSITION_ID", type: "BIGINT", remarks: "岗位ID") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "LANG", remarks: "语言", type: "VARCHAR(50)") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "NAME", remarks: "组织名称", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "组织描述", type: "VARCHAR(255)")
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
    }
    
    
    changeSet(author: "niujiaqing", id: "20160926-niujiaqing-hr-employee-assign") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'HR_EMPLOYEE_ASSIGN_S',startValue:"10001")
        }
        createTable(tableName: "HR_EMPLOYEE_ASSIGN") {
            column(autoIncrement: "true", startWith:"10001", name: "ASSIGN_ID", type: "BIGINT", remarks: "ID") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "EMPLOYEE_ID", remarks: "员工ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "POSITION_ID", remarks: "岗位ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "PRIMARY_POSITION_FLAG", remarks: "主岗位标示", type: "VARCHAR(1)")
            column(name: "ENABLED_FLAG", remarks: "启用状态", type: "VARCHAR(1)", defaultValue : "Y") {constraints(nullable: "false")}
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
        addUniqueConstraint(tableName:"HR_EMPLOYEE_ASSIGN", columnNames:"EMPLOYEE_ID, POSITION_ID", constraintName:"HR_EMPLOYEE_ASSIGN_U1")
    }
    
    
    changeSet(author: "niujiaqing", id: "20161011-niujiaqing-sys-dashboard") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'SYS_DASHBOARD_B_S',startValue:"10001")
        }
        createTable(tableName: "SYS_DASHBOARD_B") {
            column(autoIncrement: "true", startWith:"10001", name: "DASHBOARD_ID", type: "BIGINT", remarks: "ID") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "DASHBOARD_CODE", remarks: "仪表盘编码", type: "VARCHAR(100)") {
                constraints(nullable: "false",unique:"true",uniqueConstraintName:"SYS_DASHBOARD_B_U1")
            }
            column(name: "TITLE", remarks: "仪表盘标题", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "仪表盘描述", type: "VARCHAR(255)")
            column(name: "RESOURCE_ID", remarks: "资源ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "ENABLED_FLAG", remarks: "启用状态", type: "VARCHAR(1)", defaultValue : "Y") {constraints(nullable: "false")}
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")
        }
        
    	createTable(tableName: "SYS_DASHBOARD_TL") {
            column(name: "DASHBOARD_ID", type: "BIGINT", remarks: "ID") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "LANG", remarks: "语言", type: "VARCHAR(50)") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "TITLE", remarks: "仪表盘标题", type: "VARCHAR(100)")
            column(name: "DESCRIPTION", remarks: "仪表盘描述", type: "VARCHAR(255)")
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
    }
    
    changeSet(author: "niujiaqing", id: "20161012-niujiaqing-user-dashboard") {

        if(mhi.isDbType('oracle')){
            createSequence(sequenceName: 'SYS_USER_DASHBOARD_S',startValue:"10001")
        }
        createTable(tableName: "SYS_USER_DASHBOARD") {
            column(autoIncrement: "true", startWith:"10001", name: "USER_DASHBOARD_ID", type: "BIGINT", remarks: "ID") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "USER_ID", remarks: "用户ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "DASHBOARD_ID", remarks: "仪表盘ID", type: "BIGINT") {constraints(nullable: "false")}
            column(name: "DASHBOARD_SEQUENCE",type: "decimal(20,0)",defaultValue: "1", remarks: "仪表盘排序号")
            
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")
        }
    }

    changeSet(author: "xiangyuQi", id: "20161031-sys-interfaceHeader-1") {

        createTable(tableName:"SYS_IF_CONFIG_HEADER_B "){
            column(name: "HEADER_ID", type: "VARCHAR(255)", remarks: "pk") {
                constraints(nullable: "false", primaryKey: "true",primaryKeyName:"SYS_INTERFACE_HEADER_PK")
            }
            column(name:"INTERFACE_CODE",type:"VARCHAR(30)",remarks: "系统代码"){
                constraints(nullable: "false")
            }
            column(name:"INTERFACE_TYPE",type:"VARCHAR(10)",remarks: "接口类型"){
                constraints(nullable: "false")
            }
            column(name:"DOMAIN_URL",type:"VARCHAR(200)",remarks: "系统地址"){
                constraints(nullable: "false")
            }
            column(name:"BODY_HEADER",type:"VARCHAR(2000)",remarks: "SOAP报文前缀")
            column(name:"BODY_TAIL",type:"VARCHAR(2000)",remarks: "SOAP报文后缀")
            column(name:"NAMESPACE",type:"VARCHAR(30)",remarks: "SOAP命名空间")
            column(name:"REQUEST_METHOD",type:"VARCHAR(10)",remarks: "请求方法"){
                constraints(nullable: "false")
            }
            column(name:"REQUEST_FORMAT",type:"VARCHAR(30)",remarks: "请求形式"){
                constraints(nullable: "false")
            }
            column(name:"REQUEST_CONTENTTYPE",type:"VARCHAR(80)",remarks: "请求报文格式")
            column(name:"REQUEST_ACCEPT",type:"VARCHAR(80)",remarks: "请求接收类型")
            column(name:"AUTH_FLAG",type:"VARCHAR(1)",remarks: "是否需要验证"){
                constraints(nullable: "false")
            }
            column(name:"AUTH_USERNAME",type:"VARCHAR(80)",remarks: "校验用户名")
            column(name:"AUTH_PASSWORD",type:"VARCHAR(200)",remarks: "校验密码")
            column(name:"ENABLE_FLAG",type:"VARCHAR(1)",remarks: "是否有效"){
                constraints(nullable: "false")
            }
            column(name:"NAME",type:"VARCHAR(200)",remarks: "系统名称"){
                constraints(nullable: "false")
            }
            column(name:"DESCRIPTION",type:"VARCHAR(255)",remarks: "系统描述"){
                constraints(nullable: "false")
            }
            column(name:"SYSTEM_TYPE",type:"VARCHAR(10)",remarks: "系统类型")
            column(name:"MAPPER_CLASS",type:"VARCHAR(255)",remarks: "包装类")
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }

        createTable(tableName:"SYS_IF_CONFIG_HEADER_TL") {
            column(name: "HEADER_ID", type: "VARCHAR(255)", remarks: "pk") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "LANG", remarks: "语言", type: "VARCHAR(50)") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "NAME", remarks: "系统名称", type: "VARCHAR(200)")
            column(name: "DESCRIPTION", remarks: "系统描述", type: "VARCHAR(255)")

            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
    }

    changeSet(author: "xiangyuQi", id: "20161031-sys-interfaceLine-1") {

        createTable(tableName:"SYS_IF_CONFIG_LINE_B"){
            column(name: "LINE_ID", type: "VARCHAR(255)", remarks: "pk") {
                constraints(nullable: "false", primaryKey: "true",primaryKeyName:"SYS_INTERFACE_LINE_PK")
            }
            column(name:"HEADER_ID",type:"VARCHAR(255)",remarks: "行Id"){
                constraints(nullable: "false")
            }
            column(name:"LINE_CODE",type:"VARCHAR(30)",remarks: "接口代码"){
                constraints(nullable: "false")
            }
            column(name:"IFT_URL",type:"VARCHAR(200)",remarks: "接口地址"){
                constraints(nullable: "false")
            }
            column(name:"ENABLE_FLAG",type:"VARCHAR(1)",remarks: "是否有效"){
                constraints(nullable: "false")
            }
            column(name:"LINE_NAME",type:"VARCHAR(50)",remarks: "接口名称")
            column(name:"LINE_DESCRIPTION",type:"VARCHAR(255)",remarks: "接口描述")
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }

        createTable(tableName:"SYS_IF_CONFIG_LINE_TL") {
            column(name: "HEADER_ID", type: "VARCHAR(255)", remarks: "pk") {
                constraints(nullable: "false", primaryKey: "true")
            }
            column(name: "LANG", remarks: "语言", type: "VARCHAR(50)") {constraints(nullable: "false", primaryKey: "true")}
            column(name: "LINE_NAME", remarks: "接口名称", type: "VARCHAR(50)")
            column(name: "LINE_DESCRIPTION", remarks: "接口描述", type: "VARCHAR(255)")
            column(name: "OBJECT_VERSION_NUMBER", type: "BIGINT", defaultValue : "1")
            column(name: "REQUEST_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "PROGRAM_ID", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "CREATION_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATED_BY", type: "BIGINT", defaultValue : "-1")
            column(name: "LAST_UPDATE_DATE", type: "DATETIME", defaultValueComputed : "CURRENT_TIMESTAMP")
            column(name: "LAST_UPDATE_LOGIN", type: "BIGINT", defaultValue : "-1")

        }
    }

}
