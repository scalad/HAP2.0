# 消息机制

HAP 的消息系统主要是借助 Redis 实现的。并没有采用 AMQ 等 JMS 阵营的实现。

## 1.队列
***
队列的特点是：一个消息只能被消费一次，即使是在集群或者多线程环境下。

> 一般来讲，如果一个队列有多个消费者，这些消费者取到消息的概率大致是相同的。
> 大批量的消息，会大致均匀分散给所有消费者处理。

### 1.1.向队列发送一个消息

在 java 代码中，注入 IMessagePublisher 类。

```java
public class XXXSenderTest {
  @Autowired
  private IMessagePublisher messagePublisher;
  
  public void sendMessageTest (){
    // 向 队列：queue1 中发送消息 ： message1（字符串）
    messagePublisher.rPush("queue1","message1");
    
    User bean = new User();
    bean.setUserName("YJP");
    // ...
    // 向队列：queue2 中发送消息：bean（对象）
    messagePublisher.rPush("queue2",bean);
  }
}

```

以上代码示例了如何发送一个消息
1. 注入 IMessagePublisher 用于发送消息
2. rPush 方法（执行 redis 命令 RPUSH）
  * 关于队列的名字，是任意的，不需要预先定义
3. 消息类型，rPush 方法会自动判断：
  * 如果为 String 或数字，则会直接作为字符串发送
  * 其他类型，则使用 jackson 序列化后发送

> 类型的转换是透明的，调用者一般不需要关心


### 1.2.监听队列中的消息

监听队列消息的 java 实现

```java
@QueueMonitor(queue="queue1")
public XXXReceiver implements IMessageConsumer<String>{
  /**
   * 第一个参数的类型，与 IMessageConsumer 的泛型类型一致
   */
  public void onMessage(String message,String queue) {
    System.out.println("got message :"+message);
  }
}
```

这个类需要在 Spring 中定义为 bean （也可以通过 @Service 之类的注解自动扫描注册）

关键点：
1. 注解 QueueMonitor，表示这个类要监听队列 queue1
2. 接口 IMessageConsumer，提供一个方法用于接收消息，同时指定了消息类型
  * 这个接口是推荐的默认接口，但并不是必须的
  
  
  

如果消息类型是 java bean 对象，比如 1.1章节 例子中 的 User，那么需要实现的接口是IMessageConsumer< User>。

> 绝大部分时候，都要保证发送的消息类型与接收的类型一致!

## 2.发布/订阅
***
发布订阅模式的特点是：一个消息会被所有订阅者收到（只会收到一次）。

### 2.1 发布一个消息

发布一条消息的 java 代码实例

```java
public class XXXPublisher {
  @Autowired
  private IMessagePublisher messagePublisher;
  
  public void publishTest (){
    // 向频道 : channel1 发布一个字符串消息
    messagePublisher.publish("channel1","message1");
    
    User bean = new User();
    bean.setUserName("YPP");
    // 向频道：channel2 发布一个 java bean
    messagePublisher.publish("channel2",bean);
  }
}
```

基本点，与章节 1.1 类似
1. 注入 IMessagePublisher 
2. publish 方法（这个与队列不同！）
3. 支持多种类型

### 2.2 订阅一个消息

示例 java 类

```java
@TopicMonitor(channel={"channel1"})
publish class XXXSubscriber implements IMessageConsumer<String>{
  public void onMessage(String message,String pattern) {
    System.out.println("onMessage:"+message);
  }
}

```

基本点，与章节 1.2 类似
1. 注解 TopicMonitor，channel是一个数组，可以同时订阅多个频道，需要保证消息类型一致
2. 接口 IMessageConsumer，用于接收消息

这个类需要定义为 Spring bean，或者通过注解自动扫描注册。